# Docker Image 

Based on the Docker imaged developed by Phil Hawthorne:

https://github.com/philhawthorne/docker-influxdb-grafana

Changes included in this local image are:

* New Grafana Version 

* Populate default Database `tracking`

* Adding provisioned DashBoard and Datasources


docker build -t "jongzazaal/talaiot_fork:latest" .

push docker desktop


docker run -d \
  -p 80:3003 \
  -p 3004:8083 \
  -p 8086:8086 \
  -p 22022:22 \
  -v /var/lib/influxdb \
  -v /var/lib/grafana \
  jongzazaal/talaiot_fork:latest

user/pass: root/root


docker container ls
docker container rm -f
docker image ls
docker image rm jongzazaal/talaiot_fork